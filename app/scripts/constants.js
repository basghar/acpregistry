/**
 * Created by babar on 11/09/14.
 */

angular.module('acpregistryApp')
    .constant('USER_ROLES', {
        all: '*',
        admin: 'admin',
        acp: 'acp',
        guest: 'guest'
    }).constant('validateExpressions', {
        password: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
        phone: /^\(?(?:\+?61|0)(?:(?:2\)?[ -]?(?:3[ -]?[38]|[46-9][ -]?[0-9]|5[ -]?[0-35-9])|3\)?(?:4[ -]?[0-57-9]|[57-9][ -]?[0-9]|6[ -]?[1-67])|7\)?[ -]?(?:[2-4][ -]?[0-9]|5[ -]?[2-7]|7[ -]?6)|8\)?[ -]?(?:5[ -]?[1-4]|6[ -]?[0-8]|[7-9][ -]?[0-9]))(?:[ -]?[0-9]){6}|4\)?[ -]?(?:(?:[01][ -]?[0-9]|2[ -]?[0-57-9]|3[ -]?[1-9]|4[ -]?[7-9]|5[ -]?[018])[ -]?[0-9]|3[ -]?0[ -]?[0-5])(?:[ -]?[0-9]){5})$/,
        address: "((?<strPOBox>((POBox|PO\sBox)\s*\d*)),?\s?)?(((?<strUnit>([\w\d\s\,]*)),\s?)?( (?<strStreet>([\w\s\-]*\w\s(st\s)?[\w]*\s(street|st|road|rd|close|cl|avenue|ave|av|path|ph|drive|drv|LOOP|COURT|CT|CIRCLE|LANE|LN)) ),?\s?))?((?<strTown>([\p{Ll}\p{Lu}\p{Lo}\p{Pc}\p{Lt}\p{Lm}\s]*)),?\s?)?((?<strState>(Victoria|VIC|New South Wales|NSW|South Australia|SA|Northern Territory|NT|West Australia|WA|Tasmania|TAS|ACT|Queensland|QLD))\s*)?(?<strPostalCode>(\d{4}),?\s?)?(?<strCountry>(Australia))?"
    }).constant('urls', {
        acp: '/service/acp',
        register: '/service/auth/register',
        auth: '/service/auth',
        upload: '/service/admin/upload'
    }).constant('cssConstants', {
        errorClass: 'has-errors'
    });