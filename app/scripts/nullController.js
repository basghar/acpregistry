angular.module('acpregistryApp')
    .controller('NullCtrl', ['$scope', 'session', function ($scope, session) {

        $scope.searchPath = session.getSearchPath() || '';

        $scope.back = function(){
            history.go(-1);
        };

    }]);