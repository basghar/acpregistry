angular.module('acpregistryApp').directive('toggle', ['$swipe', function ($swipe) {
    "use strict";

    function updateElementClass(element, selected) {
        if (selected) {
            element.addClass('selected');
        } else {
            element.removeClass('selected');
        }
    }

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function ($scope, element, attrs, ngModel) {
            var ignoreMouseUpEvent = false;

            if(!ngModel){
                return;
            }

            ngModel.$render = function () {
                updateElementClass(element, ngModel.$viewValue);
            };

            $swipe.bind(element, {
                'end': function (coords, event) {
                    var selected = !ngModel.$viewValue;

                    //hack for android issue.
                    if(ignoreMouseUpEvent && event.type === 'mouseup'){
                        return false;
                    }

                    if (event.type === 'touchend') {
                        ignoreMouseUpEvent = true;
                    }

                    ngModel.$setViewValue(selected);
                    updateElementClass(element, selected);
                    $scope.$apply();
                }
            });
        }
    };
}]);