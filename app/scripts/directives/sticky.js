angular.module('acpregistryApp').directive('sticky', ['$window', '$swipe', function ($window, $swipe) {
    "use strict";

    function updateElementClass(element, isSticky) {
        if (isSticky) {
            element.addClass('sticky');
        } else {
            element.removeClass('sticky');
        }
    }

    return {
        restrict: 'A',
        link: function ($scope, element) {
            var next = element.next(),
                window = angular.element($window),
                elementOffset = element.offset().top;


            function fixOrUnfix(){
                if (window.scrollTop() >= elementOffset) {
                    next.css('margin-top', element.outerHeight());
                    updateElementClass(element, true);
                } else {
                    next.css('margin-top', 0);
                    updateElementClass(element, false);
                }
            }

            window.bind('scroll touchmove', fixOrUnfix);
        }
    };
}]);