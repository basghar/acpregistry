angular.module('acpregistryApp').directive('stats', ['SearchParamUtils', function (utils) {
    "use strict";

    return {
        restrict: 'EA',
        require: '?ngModel',
        replace: true,
        templateUrl: 'scripts/directives/searchStats/searchStats.html',
        link: function ($scope, element, attrs, ngModel) {
            $scope.getCriteriaText = function (fieldName) {
                var criteriaCount,
                    criteria = ngModel.$viewValue,
                    criteriaField = criteria[fieldName];

                if (angular.isObject(criteriaField)) {
                    criteriaCount = utils.getSelectedOptionCount(criteriaField);
                    return criteriaCount > 0 ? criteriaCount : 'All';
                } else {
                    return (criteriaField) ? criteriaField : 'All';
                }
            };

        }
    };
}]);