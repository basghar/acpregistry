angular.module('acpregistryApp').directive('wordcount', function (){
    return {
        require: 'ngModel',
        link: function(scope, elem, attr, ngModel) {
            var regex = /\s+/gi,
                wordcount = scope.$eval(attr.wordcount);

            function isValid(value){
                return !value || value.trim().replace(regex, ' ').split(' ').length <= wordcount;
            }

            //For DOM -> model validation
            ngModel.$parsers.unshift(function(value) {
                var valid = isValid(value);
                ngModel.$setValidity('wordcount', valid);
                return valid ? value : undefined;
            });

            //For model -> DOM validation
            ngModel.$formatters.unshift(function(value) {
                ngModel.$setValidity('wordcount', isValid(value));
                return value;
            });
        }
    };
});