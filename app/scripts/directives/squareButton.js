angular.module('acpregistryApp').directive('squareButton', function () {
    "use strict";

    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        templateUrl: 'scripts/directives/squareButton.html',
        scope: {
            icon: '@'
        }
    };
});
