angular.module('acpregistryApp').directive('optionBar', function () {
    "use strict";

    return {
        restrict: 'EA',
        require: '?ngModel',
        transclude: true,
        replace: true,
        templateUrl: 'scripts/directives/optionBar/optionBar.html',
        scope: {
            option: '=ngModel'
        }
    };
});
