angular.module('acpregistryApp').run(['$rootScope', '$location', '$anchorScroll', '$injector', 'authService', 'session',
    function ($rootScope, $location, $anchorScroll, $injector, authService, session) {

        function isTermsRoute(route) {
            return (angular.isString(route) && route.endsWith('/terms')) || route.isTermsRoute;
        }

        $rootScope.$on("$routeChangeStart", function (event, next) {
            if (!isTermsRoute(next) && !authService.hasAcceptedTerms()) {
                session.lastUrl = $location.path();
                $location.path('/terms');
            } else if(next.$$route){
                if (next.$$route.accessController) {
                    var access,
                        getAccess = $injector.get(next.$$route.accessController);

                    access = getAccess(next.params, authService.getLoggedInUser());

                    if (!access.allowed) {
                        if (access.redirect) {
                            $location.path(access.redirect);
                        } else {
                            $location.path('/');
                        }
                    }
                } else if (next.$$route.accessibleTo) {
                    var accessibleToRoles = accessibleTo,
                        loggedInUser = authService.getLoggedInUser();

                    if (!loggedInUser || accessibleToRoles.indexOf(loggedInUser.role) < 0) {
                        $location.path('/');
                    }
                }
            }
        });
    }
]);