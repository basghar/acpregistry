'use strict';

/**
 * @ngdoc overview
 * @name acpregistryApp
 * @description
 * # acpregistryApp
 *
 * Main module of the application.
 */
angular
    .module('acpregistryApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngStorage',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'angular-loading-bar',
        'ui.bootstrap',
        'angularFileUpload'
    ]).config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }]).config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider.when('/:postcode/:sectors/:techs/:companyName/:calcMethods', {
            templateUrl: '../components/search/search.html',
            controller: 'SearchCtrl'
        }).when('/result/:postcode/:sectors/:techs/:companyName/:calcMethods', {
            templateUrl: '../components/result/result.html',
            controller: 'ResultCtrl'
        }).when('/detail/:cn', {
            templateUrl: '../components/detail/detail.html',
            controller: 'DetailCtrl'
        }).when('/detail/edit/:cn', {
            templateUrl: '../components/detail/edit/edit.html',
            controller: 'DetailEditCtrl',
            accessController: 'DetailEditAccess'
        }).when('/register', {
            templateUrl: '../components/user/register.html',
            controller: 'RegisterCtrl'
        }).when('/login', {
            templateUrl: '../components/user/login.html',
            controller: 'LoginCtrl'
        }).when('/password', {
            templateUrl: '../components/user/password.html',
            controller: 'PasswordCtrl'
        }).when('/terms', {
            templateUrl: '../components/terms/terms.html',
            controller: 'TermsCtrl',
            isTermsRoute: true
        }).when('/help', {
            templateUrl: '../components/help/help.html',
            controller: 'NullCtrl'
        }).when('/admin/upload', {
            templateUrl: '../components/admin/upload/upload.html',
            controller: 'AdminUploadCtrl'
        }).when('/admin/uploadResult', {
            templateUrl: '../components/admin/upload/uploadResult.html',
            controller: 'AdminUploadResultCtrl'
        }).otherwise({
            redirectTo: '/all/all/all/all/all'
        });

    }]).run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {

        $rootScope.mainTitle = 'ACP Directory';

        var original = $location.path;
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };
    }]).run(['$rootScope', '$location', '$anchorScroll', 'authService', 'session', 'SearchParamUtils', 'USER_ROLES',
        function ($rootScope, $location, $anchorScroll, authService, session, utils, ROLES) {

            if (typeof String.prototype.endsWith !== 'function') {
                String.prototype.endsWith = function (suffix) {
                    return this.indexOf(suffix, this.length - suffix.length) !== -1;
                };
            }

            if (typeof String.prototype.toTitleCase !== 'function') {
                String.prototype.toTitleCase = function () {
                    return this.charAt(0).toUpperCase() + this.substr(1).toLowerCase();
                };
            }

            $rootScope.$on("$routeChangeSuccess", function (event, current) {
                if (current.$$route && current.$$route.initialAnchor) {
                    $location.hash(current.$$route.initialAnchor);
                    $anchorScroll();
                }
            });
        }
    ]);