angular.module('acpregistryApp').factory('session', ['$sessionStorage', function ($sessionStorage) {
    'use strict';

    var lastUrl,
        Session = {
            lastUrl: null
        };

    Session.setSearchPath = function (searchPath) {
        $sessionStorage.searchPath = searchPath;
    };

    Session.getSearchPath = function(){
        return $sessionStorage.searchPath;
    };

    Session.setCriteria = function (criteria) {
        $sessionStorage.criteria = criteria;
    };

    Session.getCriteria = function(){
        return $sessionStorage.criteria;
    };

    Session.clear = function (){
        delete $sessionStorage.criteria;
    };

    return Session;
}]);