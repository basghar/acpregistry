'use strict';

angular.module('acpregistryApp').factory('SearchParamUtils', ['staticData', function (staticData) {
    var ALL = 'all',
        SearchParamUtils = {};

    SearchParamUtils.ALL = ALL;

    SearchParamUtils.getSearchTermFromPathElement = function (pathElement) {
        return (!pathElement || pathElement === ALL) ? '' : pathElement;
    };

    SearchParamUtils.getPathElementFromSearchTerm = function (searchTerm) {
        return (searchTerm ? searchTerm : ALL);
    };

    SearchParamUtils.getPathElementFromObject = function (object, refs) {
        var key,
            pathElement = '';

        for (var i = 0; i < refs.length; i++) {
            key = refs[i].key;
            if (object[key]) {
                if (pathElement) {
                    pathElement += ':' + key;
                } else {
                    pathElement = key;
                }
            }
        }

        return (pathElement ? pathElement : ALL);
    };

    SearchParamUtils.getSearchPath = function (criteria) {
        return '/' +
            SearchParamUtils.getPathElementFromSearchTerm(criteria.postcode) + '/' +
            SearchParamUtils.getPathElementFromObject(criteria.sectors, staticData.sectors) + '/' +
            SearchParamUtils.getPathElementFromObject(criteria.techs, staticData.techs) + '/' +
            encodeUriElement(SearchParamUtils.getPathElementFromSearchTerm(criteria.companyName)) + '/' +
            SearchParamUtils.getPathElementFromObject(criteria.calcMethods, staticData.calcMethods);
    };

    function createAttributes(object, value) {
        if (value) {
            angular.forEach(value.split(':'), function (attr) {
                object[attr] = true;
            });
        }
    }

    SearchParamUtils.createCriteria = function (params) {
        var params = params || {},
            criteria = {
                postcode: SearchParamUtils.getSearchTermFromPathElement(params.postcode),
                sectors: {},
                techs: {},
                companyName: SearchParamUtils.getSearchTermFromPathElement(params.companyName),
                calcMethods: {}
            };

        if (criteria.postcode) {
            criteria.postcode = parseInt(criteria.postcode);
        }

        if (params.sectors) {
            createAttributes(criteria.sectors, params.sectors);
        }

        if (params.techs) {
            createAttributes(criteria.techs, params.techs);
        }

        if (params.calcMethods) {
            createAttributes(criteria.calcMethods, params.calcMethods);
        }

        return criteria;
    };

    function encodeUriElement(value) {
        return escape(value.replace('/', '%2F'));
    }

    SearchParamUtils.encodeUriElement = encodeUriElement;

    SearchParamUtils.createEditDetailUrl = function (provider) {
        return '/detail/edit/' + encodeUriElement(angular.isString(provider) ? provider : provider.companyName);
    };

    SearchParamUtils.createDetailUrl = function (provider) {
        var name = encodeUriElement(angular.isString(provider) ? provider : provider.companyName);
        return '/detail/' + escape(name);
    };

    function isParticularValueSelected(object) {
        for (var key in object) {
            if (object.hasOwnProperty(key) && key !== 'all' && object[key]) {
                return true;
            }
        }
        return false;
    }

    SearchParamUtils.getSelectedOptionCount = function (options) {
        var count = 0;

        for (var key in options) {
            if (options.hasOwnProperty(key) && key !== 'all' && options[key]) {
                count++;
            }
        }
        return count;
    };

    SearchParamUtils.isAnyValueSelected = isParticularValueSelected;

    SearchParamUtils.updateObjectAllState = function (object, allChanged) {
        if (allChanged) {
            if (!angular.isDefined(object.all) || object.all) {
                object = {all: true};
            }
        } else {
            object.all = !isParticularValueSelected(object);
        }
        return object;
    };

    return SearchParamUtils;
}]);