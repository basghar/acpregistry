angular.module('acpregistryApp').factory('staticData', [function () {
        'use strict';

        var staticData = {};

        function createMap(name, data) {
            var map = {};
            for (var i = 0; i < data.length; i++) {
                map[data[i].key] = data[i].name;
            }

            staticData[name + 's'] = data;
            staticData['get' + name.charAt(0).toUpperCase() + name.substr(1)] = function(key){
                return map[key];
            };

            return map;
        }

        createMap('sector', [
            {key: 'residential', name: 'residential'},
            {key: 'smallbiz', name: 'Small Business'},
            {key: 'commercial', name: 'Commercial'},
            {key: 'industrial', name: 'Industrial'}
        ]);

        createMap('tech', [
            {key: 'lighting', name: 'Lighting'},
            {key: 'aircon', name: 'Air heating and cooling'},
            {key: 'refrigeration', name: 'Refrigeration and freezing'},
            {key: 'waterheating', name: 'Water heating'},
            {key: 'transport', name: 'Transport'},
            {key: 'entertainment', name: 'Home entertainment'},
            {key: 'office', name: 'Computers, office, equipment'},
            {key: 'processheat', name: 'Process heat'},
            {key: 'aircompression', name: 'Air compression'},
            {key: 'ventilation', name: 'Air handling, fans ventilation'},
            {key: 'conveying', name: 'Materials handling conveying'},
            {key: 'mixing', name: 'Milling mixing grinding'},
            {key: 'comm', name: 'Communications'},
            {key: 'cleaning', name: 'Cleaning Washing'},
            {key: 'processdrive', name: 'Process drives'},
            {key: 'electricity', name: 'Electricity supply'},
            {key: 'other', name: 'Other machine'},
            {key: 'escalators', name: 'People movement lifts, escalators'},
            {key: 'cooking', name: 'Cooking'},
            {key: 'pumping', name: 'Water / Liquid pumping'}
        ]);

        createMap('calcMethod', [
            {key: 'pia', name: 'Project Impact Assessment Method'},
            {key: 'piamv', name: 'Project Impact Assessment with Measurement and Verification Method'},
            {key: 'c85', name: 'Baseline per unit of output (clause 8.5)'},
            {key: 'c86', name: 'Baseline unaffected by output (clause 8.6)'},
            {key: 'c87', name: 'Normalized baselines (clause 8.7)'},
            {key: 'c88', name: 'NABERS baseline (clause 8.8)'},
            {key: 'c89', name: 'Aggregated Metered Baseline (clause 8.9)'},
            {key: 'c93', name: 'Sales of New Appliances (clause 9.3)'},
            {key: 'c94', name: 'Commercial Lighting Energy Savings Formula (clause 9.4)'},
            {key: 'c95', name: 'High Efficiency Motor Energy Savings Formula (clause 9.5)'},
            {key: 'c96', name: 'Power Factor Correction Energy Savings Formula (clause 9.6)'},
            {key: 'c97', name: 'Removal of Old Appliances (clause 9.7)'},
            {key: 'c98', name: 'Home Energy Efficiency Retrofits (clause 9.8)'},
            {key: 'c99', name: 'Installation of High Efficiency Appliances for Businesses (clause 9.9)'},
            {key: 'c910', name: "'1 for 1' Residential Downlight Replacement (clause 9.10)"}

        ]);

        return staticData;
    }]);