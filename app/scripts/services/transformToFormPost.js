angular.module('acpregistryApp').factory("transformToFormPost", function () {

        function serializeData(data) {

            if (!angular.isObject(data)) {
                return( ( data == null ) ? "" : data.toString() );
            }

            var buffer = [];

            // Serialize each key in the object.
            for (var name in data) {
                if (data.hasOwnProperty(name)) {
                    var value = data[ name ];

                    buffer.push(
                            encodeURIComponent(name) +
                            "=" +
                            encodeURIComponent(( value == null ) ? "" : value)
                    );
                }
            }

            return buffer.join("&").replace(/%20/g, "+");
        }

        function transformRequest(data, getHeaders) {
            getHeaders()[ "Content-Type" ] = "application/x-www-form-urlencoded; charset=utf-8";
            return serializeData(data);
        }

        return transformRequest;
    }
);
 