angular.module('acpregistryApp').factory('ACPData', ['$http', '$q', 'staticData', 'urls', 'SearchParamUtils',
    function ($http, $q, staticData, urls, utils) {
        'use strict';
        var ACPDataService = {},
            providers;

        providers = $http.get(urls.acp).then(function (response) {
            providers = response.data;
            return providers;
        });

        function containsIn(cFieldObject, fieldName, certificates) {
            var i, key,
                contains = true;

            main:
                for (key in cFieldObject) {
                    if (key != 'all' && cFieldObject.hasOwnProperty(key) && cFieldObject[key]) {
                        contains = false;
                        for (i = 0; i < certificates.length; i++) {
                            if (key === certificates[i][fieldName]) {
                                contains = true;
                                break main;
                            }
                        }
                    }
                }
            return contains;
        }

        function isDataAvailable() {
            return providers && angular.isArray(providers);
        }

        ACPDataService.getData = function () {
            return $q.when(providers);
        };

        ACPDataService.getACPDetailForEdit = function (companyName) {
            return $http.get(urls.acp + '/' + utils.encodeUriElement(companyName)).then(function(response){
                return response.data;
            }, function(response){
                var errorMessage;

                switch (response.status) {
                case 404:
                    errorMessage = 'Could not find a company with name ' + companyName;
                    break;
                case 403:
                    errorMessage = "Access denied";
                    break;
                default:
                    errorMessage = 'Service Error';
                }
                return $q.reject(errorMessage);
            });
        };

        function createRegexExpr(value) {
            return new RegExp(value.replace('*', '.*').replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"), 'i');
        }

        ACPDataService.createFilter = function (criteria) {
            var nameRegex = criteria.companyName ? createRegexExpr(criteria.companyName) : undefined;

            return function (provider) {
                var certificates = provider.certificates;
                return (criteria.postcode ? provider.postcodesServed.indexOf(criteria.postcode) >= 0 : true) &&
                    containsIn(criteria.sectors, 'sector', certificates) &&
                    containsIn(criteria.techs, 'projectTechnologyType', certificates) &&
                    (nameRegex ? nameRegex.test(provider.companyName) : true) &&
                    containsIn(criteria.calcMethods, 'calculationMethod', certificates);
            };
        };

        ACPDataService.filter = function (c) {
            var i, filtered;

            if (isDataAvailable()) {
                if (!angular.isFunction(c)) {
                    c = ACPDataService.createFilter(c);
                }

                filtered = [];
                for (i = 0; i < providers.length; i++) {
                    if (c(providers[i])) {
                        filtered.push(providers[i]);
                    }
                }
            }

            return filtered;
        };

        ACPDataService.findByName = function (name) {
            var i = 0,
                matched;

            if (isDataAvailable()) {
                for (; i < providers.length; i++) {
                    if (providers[i].companyName === name) {
                        matched = providers[i];
                        break;
                    }
                }
            }

            return matched;
        };

        return ACPDataService;
    }

])
;



