'use strict';

angular.module('acpregistryApp').factory('ga', ['$filter', 'ACPData', 'SearchParamUtils',
    function ($filter, acpData, utils) {
        var GAService = {},
            filter = $filter('filter');

        GAService.sendPageView = function (criteria, path) {
            var filteredList = filter(acpData.getData(), utils.matchesACP(criteria));

            if (ga) {
                ga('set', 'dimension1', utils.getPathElementFromSearchTerm(criteria.postcode));
                ga('set', 'dimension2', utils.getPathElementFromSearchTerm(criteria.companyName));
                ga('set', 'dimension3', utils.getPathElementFromSectors(criteria));
                ga('set', 'metric1', filteredList.length);
                ga('send', 'pageview', {
                    'page': path,
                    'title': 'ACP Search Filter',
                    'hitCallback': function () {
                        console.log('analytics.js done sending pageview with page ' + path);
                    }
                });
            }

        };

        return GAService;
    }]);



