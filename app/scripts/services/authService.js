angular.module('acpregistryApp').factory('authService', ['$sessionStorage', '$localStorage', '$http', '$q',
    'urls', 'transformToFormPost', '$$authInjector',
    function ($sessionStorage, $localStorage, $http, $q, urls, transformToFormPost, $$authInjector) {
        'use strict';

        var AuthService = {};

        if ($localStorage.u) {
            $$authInjector.login($localStorage.u);
        } else if ($sessionStorage.u) {
            $$authInjector.login($sessionStorage.u);
        }

        AuthService.isLoggedIn = $$authInjector.isLoggedIn;
        AuthService.getLoggedInUser = $$authInjector.getLoggedInUser;

        AuthService.login = function (user) {
            return $http.post(urls.auth, {
                userid: user.email,
                password: user.password
            }, {
                transformRequest: transformToFormPost
            }).then(function (response) {
                var loggedInUser = response.data;
                $$authInjector.login(loggedInUser);
                if (user.rememberme) {
                    $localStorage.u = loggedInUser;
                } else {
                    $sessionStorage.u = loggedInUser;
                }
                return loggedInUser;
            }, function (response) {
                var errorMessage;

                switch (response.status) {
                case 404:
                case 403:
                    errorMessage = 'Email address or password is incorrect';
                    break;
                default:
                    errorMessage = 'Service Error';
                }
                return $q.reject(errorMessage);
            });
        };

        AuthService.logout = function () {
            if ($$authInjector.isLoggedIn()) {
                return $http.delete(urls.auth).then(function (response) {
                    if ($localStorage.u) {
                        delete $localStorage.u;
                    } else if ($sessionStorage.u) {
                        delete $sessionStorage.u;
                    }
                    $$authInjector.logout();
                    return response;
                });
            }
        };

        AuthService.hasAcceptedTerms = function () {
            if (!angular.isDefined($localStorage.acceptedTerms)) {
                $localStorage.acceptedTerms = false;
            }

            return $localStorage.acceptedTerms;
        };

        AuthService.markTermsAsAccepted = function () {
            $localStorage.acceptedTerms = true;
        };

        return AuthService;
    }]).service('$$authInjector',
    function () {

        var loggedInUser;

        return {
            login: function (user) {
                loggedInUser = user;
            },
            logout: function () {
                loggedInUser = null;
            },
            isLoggedIn: function () {
                return !!(loggedInUser);
            },
            getLoggedInUser: function () {
                return loggedInUser;
            },
            request: function (config) {
                if (loggedInUser) {
                    config.headers['session-token'] = loggedInUser.token;
                }
                return config;
            }
        };
    }).config(['$httpProvider',
        function ($httpProvider) {
            $httpProvider.interceptors.push('$$authInjector');
        }
    ]);