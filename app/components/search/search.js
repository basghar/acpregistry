'use strict';

/**
 * @ngdoc function
 * @name acpregistryApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Controller of the acpregistryApp
 */
angular.module('acpregistryApp')
    .controller('SearchCtrl', ['$scope', '$routeParams', '$location',
        '$timeout', 'SearchParamUtils', 'ACPData', 'session', 'ga',
        function ($scope, $routeParams, $location, $timeout, utils, acpDataService, session, ga) {
            var filteredData,
                delayedTracker;

            $scope.isByCompanyCollapsed = true;
            $scope.isByCalculationCollapsed = true;

            function updateUri(criteria, recordGA) {
                var searchPath = utils.getSearchPath(criteria),
                    currentPath = $location.path();

                if (currentPath !== searchPath) {
                    $location.path(searchPath, false);
                }

                if (recordGA) {
                    //ga.sendPageView(criteria, searchPath);
                }

                session.setSearchPath(searchPath);

                filteredData = acpDataService.filter(criteria);
                if(filteredData){
                    $scope.criteria.filteredCount = filteredData.length;
                }
            }

            function track(criteria) {
                if (delayedTracker) {
                    $timeout.cancel(delayedTracker);
                }

                delayedTracker = $timeout(function () {
                    updateUri(criteria, true);

                    delayedTracker = undefined;
                }, 600);

            }

            $scope.criteria = utils.createCriteria($routeParams);

            $scope.$watch('criteria', function (criteria) {
                if (criteria) {
                    track(criteria);
                }
            }, true);

            $scope.isAnyValueSelected = utils.isAnyValueSelected;

            acpDataService.getData().then(function(providers){
                $scope.providers = providers;
                updateUri($scope.criteria);
            });

            $scope.showResult = function () {
                var url;

                if (filteredData) {
                    if (filteredData.length == 1) {
                        url = utils.createDetailUrl(filteredData[0]);
                    } else if (filteredData.length > 1) {
                        url = '/result' + utils.getSearchPath($scope.criteria);
                    }

                    if (url) {
                        $location.url(url);
                    }
                }
            };

            $scope.clear = function (criteria){
                var newCriteria = utils.createCriteria();
                newCriteria.filteredCount = criteria.filteredCount;
                $scope.criteria = newCriteria;
            };

            $scope.updateAllState = function (attrName, allChanged) {
                $scope.criteria[attrName] = utils.updateObjectAllState($scope.criteria[attrName], allChanged);
            };

        }]);
