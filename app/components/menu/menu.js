angular.module('acpregistryApp')
    .controller('MenuCtrl', ['$scope', '$rootScope', '$location', 'authService', 'SearchParamUtils', 'USER_ROLES',
        function ($scope, $rootScope, $location, authService, utils, ROLES) {

            $scope.ROLES = ROLES;
            $scope.user = authService.getLoggedInUser();

            $rootScope.$on('login', function (event, loggedInUser) {
                $scope.user = loggedInUser;
            });

            $scope.isACP = function(){
                var user = $scope.user;
                return user && user.role === ROLES.acp;
            };

            $scope.isAdmin = function(){
                var user = $scope.user;
                return user && user.role === ROLES.admin;
            };

            $scope.logout = function () {
                authService.logout().then(function () {
                    delete $scope.user;
                    $location.url('/');
                });
            };

            $scope.getMyACPUrl = function () {
                var user = $scope.user;
                if(user && user.companyName){
                    return utils.createEditDetailUrl(user.companyName);
                }
            }

        }
    ]);
