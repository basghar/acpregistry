angular.module('acpregistryApp')
    .controller('DetailEditCtrl', ['$scope', '$routeParams', '$location', '$rootScope', 'ACPData', 'SearchParamUtils',
        'session', 'staticData', 'validateExpressions',
        function ($scope, $routeParams, $location, $rootScope, acpData, utils, session, staticData,
                  validateExpressions) {

            $scope.searchPath = session.getSearchPath();
            $scope.phonePattern = validateExpressions.phone;
            $scope.addressPattern = validateExpressions.address;

            function tempWA(company){
                company.loginEmail = 'admin@company';

                company.councilsServed = [
                    {
                        "name": "Central West",
                        "councils": [
                            {
                                "name": "BATHURST REGIONAL COUNCIL",
                                "isServed": true
                            },
                            {
                                "name": "BLAND SHIRE COUNCIL",
                                "isServed": false
                            },
                            {
                                "name": "BLAYNEY SHIRE COUNCIL",
                                "isServed": false
                            },
                            {
                                "name": "CABONNE SHIRE COUNCIL",
                                "isServed": true
                            }
                        ]
                    },
                    {
                        "name": "Far West",
                        "councils": [
                            {
                                "name": "BROKEN HILL CITY COUNCIL",
                                "isServed": true
                            },
                            {
                                "name": "CENTRAL DARLING SHIRE COUNCIL",
                                "isServed": true
                            },
                            {
                                "name": "UNINCORPORATED FAR WEST",
                                "isServed": false
                            }
                        ]
                    }
                ];
            }

            acpData.getACPDetailForEdit($routeParams.cn).then(function (company) {
                tempWA(company);

                $scope.company = company;
                $scope.selectedRegion = company.councilsServed[0];
            }, function(errorMessage){
                alert(errorMessage);
            });

            $scope.techs = staticData.techs;
            $scope.getTech = staticData.getTech;
            $scope.getSector = staticData.getSector;
            $scope.getCalcMethod = staticData.getCalcMethod;

            function getSelectedCount(region) {
                var i,
                    selectedCount = 0,
                    councils = region.councils;
                for (i = 0; i < councils.length; i++) {
                    if (councils[i].isServed) {
                        selectedCount++;
                    }
                }

                return selectedCount;
            }

            $scope.getSelectedCount = getSelectedCount;

            $scope.selectRegion = function (region) {
                $scope.selectedRegion = region;
                $scope.councilVisible = true;
            };

            $scope.toggleRegion = function (region) {
                var i,
                    councils = region.councils,
                    selected = region.councils.length == getSelectedCount(region);

                $scope.selectedRegion = region;

                for (i = 0; i < councils.length; i++) {
                    councils[i].isServed = !selected;
                }
            };

            $scope.setRegionBtnLabel = function (region) {
                var label = 'None',
                    selectedCount = getSelectedCount(region);

                if (region.councils.length == selectedCount) {
                    label = 'All';
                } else if (selectedCount > 0) {
                    label = selectedCount;
                }

                return label;
            };

            $scope.setRegionBtnIcon = function (region) {
                var iconClass = 'fa-circle-thin',
                    selectedCount = getSelectedCount(region);

                if (region.councils.length == selectedCount) {
                    iconClass = 'fa-check-circle';
                } else if (selectedCount > 0) {
                    iconClass = 'fa-check-circle-o';
                }

                return iconClass;
            };

            $scope.update = function () {
                $scope.showErrors = true;
            };

            $scope.cancel = function () {
                history.go(-1);
            };

            $scope.$on('$locationChangeStart', function (event) {
                if ($scope.form.$dirty) {

                    if (!confirm("Your updates are not saved, Are you sure you want to leave this page? " +
                        "Press OK to leave page, cancel to stay on page")) {
                        event.preventDefault();
                    }
                }
            });

            function convertTo2DimArray(data) {
                var i, tdData = [], row;

                for (i = 0; i < data.length; i++) {
                    if (i + 1 < data.length) {
                        tdData.push([data[i], data[++i]]);
                    } else {
                        tdData.push([data[i]]);
                    }
                }

                return tdData
            }

            function convertTo1DimArray(data) {
                var i, sdData = [], row;
                for (i = 0; i < data.length; i++) {
                    row = data[i];
                    sdData.push(row[0]);
                    if (row.length > 1) {
                        sdData.push(row[1]);
                    }

                }
                return sdData;
            }

        }
    ]);
