angular.module('acpregistryApp').factory('DetailEditAccess', ['USER_ROLES', 'SearchParamUtils',
    function (ROLES, utils) {
        return function (params, loggedInUser) {
            var access = {},
                allowed = true,
                cn = params.cn;
            if (loggedInUser) {
                if (loggedInUser.role === ROLES.acp && loggedInUser.companyName !== cn) {
                    allowed = false;
                }
            } else {
                allowed = false;
            }

            if (!allowed) {
                access.redirect = utils.createDetailUrl(cn);
            }

            access.allowed = allowed;

            return access;
        };
    }]);
