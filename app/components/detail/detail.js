angular.module('acpregistryApp')
    .controller('DetailCtrl', ['$scope', '$routeParams', 'ACPData', 'SearchParamUtils', 'session', 'staticData',
        function ($scope, $routeParams, acpData, utils, session, staticData) {
            $scope.searchPath = session.getSearchPath() || '';
            acpData.getData().then(function () {
                var company = acpData.findByName($routeParams.cn);
                if (company) {
                    $scope.company = company;
                }

                $scope.certificatesSummary = _(company.certificates).groupBy('projectTechnologyType').map(function (group,
                                                                                                                    groupBy) {
                    return {
                        groupBy: groupBy,
                        total: _(group).reduce(function (total, groupElement) {
                            return total + groupElement.certificatesTotal;
                        }, 0)
                    };
                }).value();

                $scope.getTech = staticData.getTech;
            });
        }
    ]);
