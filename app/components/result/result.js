angular.module('acpregistryApp')
    .controller('ResultCtrl', ['$scope', '$routeParams', '$location', '$q', 'ACPData', 'SearchParamUtils', 'session',
        function ($scope, $routeParams, $location, $q, acpData, utils, session) {
            var criteria = utils.createCriteria($routeParams);

            $scope.searchPath = utils.getSearchPath(criteria);
            $scope.criteria = criteria;
            $scope.resultEmailLink = encodeURI("mailto:?subject=Link to ACP Search Result&body="
                + $location.absUrl());

            acpData.getData().then(function(){
                var filteredData = acpData.filter(criteria);
                $scope.acps = filteredData;
                criteria.filteredCount = filteredData.length;
            });

            $scope.createDetailUrl = utils.createDetailUrl;

            $scope.showDetails = function (company) {
                $location.url(utils.createDetailUrl(company));
            };
        }]);