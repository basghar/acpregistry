angular.module('acpregistryApp')
    .controller('PasswordCtrl', ['$scope', 'validateExpressions', function ($scope, validateExpressions) {

        $scope.title = 'Set your password';

        $scope.passwordPattern = validateExpressions.password;

        $scope.changePassword = function () {
            $scope.showErrors = true;
        };

        $scope.cancel = function () {
            history.go(-1);
        };
    }]);



