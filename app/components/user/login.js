angular.module('acpregistryApp')
    .controller('LoginCtrl',
    ['$scope', '$rootScope', 'authService', function ($scope, $rootScope, authService) {

        $scope.user = {};

        $scope.login = function () {
            var errorMessage;

            $scope.showErrors = true;

            if ($scope.form.$valid) {
                authService.login($scope.user).then(function(user){
                    $rootScope.$emit('login', user);
                    history.go(-1);
                }, function(errorMessage){
                    $scope.errorMessage = errorMessage;
                });
            }else{
                errorMessage = 'Validation error(s), please review and try again';
            }

            $scope.errorMessage = errorMessage;
        };

        $scope.cancel = function () {
            history.go(-1);
        };
    }]);

