angular.module('acpregistryApp')
    .controller('RegisterCtrl', ['$scope', '$location', function ($scope, $location) {
        $scope.register = function () {
            $scope.showErrors = true;
        };

        $scope.cancel = function () {
            history.go(-1);
        };
    }]);
