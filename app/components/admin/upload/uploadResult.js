angular.module('acpregistryApp')
    .controller('AdminUploadResultCtrl', ['$scope', 'session', 'SearchParamUtils',
        function ($scope, session, utils) {
            $scope.uploadResult = session.lastUploadResult;
            $scope.createDetailUrl = utils.createDetailUrl;
        }
    ]);
