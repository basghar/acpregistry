angular.module('acpregistryApp')
    .controller('AdminUploadCtrl', ['$scope', '$location', 'session', 'FileUploader', 'urls',
        function ($scope, $location, session, FileUploader, urls) {
            var uploader = $scope.uploader = new FileUploader({
                url: urls.upload
            });

            uploader.onSuccessItem = function(fileItem, response) {
                session.lastUploadResult = response;
                $location.url('/admin/uploadResult');
            };

            uploader.onErrorItem = function(fileItem, response, status) {
                console.info('onErrorItem', fileItem, response, status);
            };


//            uploader.filters.push({
//                name: 'csvFilter',
//                fn: function (item) {
//                    return item.name.toLowerCase().endsWith('csv');
//                }
//            });


            $scope.searchPath = session.getSearchPath();

            $scope.canUpload = function(){
                return !uploader.getNotUploadedItems().length;
            };

            $scope.upload = function(){
                uploader.uploadAll();
            };
        }
    ]);
