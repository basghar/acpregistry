angular.module('acpregistryApp')
    .controller('TermsCtrl', ['$scope', '$location', 'session', 'authService',
        function ($scope, $location, session, authService) {
            $scope.markTermsAsAccepted = function () {
                authService.markTermsAsAccepted();
                $location.path(session.lastUrl ? session.lastUrl : '/');
                session.lastUrl = null;
            };
        }]);